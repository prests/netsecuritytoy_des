import sys
import random

def binaryConvert(val):#Simple interger to binary array converter
    arr = []
    val = bin(val)[2:]
    for i in range(0,len(val)):
        arr.append(int(val[i]))
    return arr
    

def substitutionMatrix(value, type):#S-Box process
    temp = 0

    col = str(value[1]) + str(value[2])#Bit 2 and 3 combine
    col = int(col, 2)
    row = str(value[0]) + str(value[3])#Bit 1 and 4 combine
    row = int(row, 2)

    if(type == 0):
        s0 = [[1,0,3,2],[3,2,1,0],[0,2,1,3],[3,1,3,2]]
        temp = s0[row][col] #Pick value at combined bit value
    else:
        s1 = [[0,1,2,3],[2,0,1,3],[3,0,1,0],[2,1,0,3]]
        temp = s1[row][col] #Pick value at combined bit value

    arr = binaryConvert(temp) #Convert interger back to binary array

    if(len(arr) == 1 and arr[0]==0): #Padding if integer value was 0
        arr.append(0)
    elif(len(arr) == 1 and arr[0]==1): #Padding if integer value was 1
        arr.insert(0, 0)
    return arr

def keyFunction(left, right, key): #Process of each cycle
    temp = expansionPermutation(right) #Expand bits to be same size as key
    temp = xor(temp, key) #XOR key with expanded bits

    #Split bits in half
    r0 = temp[0:(len(temp)/2)]
    r1 = temp[(len(temp)/2):] 

    #put each half through its own S-Box
    r0 = substitutionMatrix(r0,0) #Use S-Box 0
    r1 = substitutionMatrix(r1,1) #Use S-Box 1

    temp = r0+r1 #Combine arrays
    temp = permutation(temp,2) #Permutation of 4 bits

    left = xor(temp, left)#XOR new right side with left side
    return right, left#Switch old right side with new left side

def leftShift(arr): #Shifts all the bits to the left and wraps first bit to the last bit
    newArr = []
    for i in range(0,len(arr)):
        if(i==len(arr)-1):
            newArr.append(arr[0])
        else:
            newArr.append(arr[i+1])
    return newArr

def xor(x1,x2): #XOR compares bits at same index
    output = []
    for i in range(0,len(x1)):
        if(x1[i] == x2[i]): #If bits are the same then output at that index 0
            output.append(0)
        else: #If bits are different then output at that index 1
            output.append(1)
    return output
  
def permutation(arr, type): #Permutation changes order of bits
    initialPermutationOrder = [1,5,2,0,3,7,4,6]
    inversePermutation = [3,0,2,4,6,1,7,5]
    p10Permutation = [2,4,1,6,3,9,0,8,7,5]
    p4Permutation = [1,3,2,0]
    permuArr = []
    order = []

    if(type==0):#Initial Permutation
        order = initialPermutationOrder
    elif(type==1):#Permutation of Key at 10bits
        order = p10Permutation
    elif(type==2):#Permutation for 4 bits
        order = p4Permutation
    elif(type==3):#Inverse of Initial Permutation
        order = inversePermutation
    else:
        print('ERROR SELECTING PERMUTATION')

    for i in range(0,len(arr)):
        permuArr.append(arr[order[i]])
    return permuArr

def condensePermutation(arr): #Condense Permutation takes the 10bits and creates 8bits
    p8Permutation = [5,2,6,3,7,4,9,8]
    permutated = []
    for i in range(0,len(p8Permutation)): #Add bits in order of the predetermined order
        permutated.append(arr[p8Permutation[i]])
    return permutated

def expansionPermutation(arr): #Expansive Permutation has two arays to make one whole size
    orderLeft = [3,0,1,2]
    orderRight = [1,2,3,0]
    arrLeft = []
    arrRight = []

    for i in range(0,len(arr)): #Add bits in order of the predetermined order
        arrLeft.append(arr[orderLeft[i]])
        arrRight.append(arr[orderRight[i]])

    permutated = arrLeft + arrRight #Combine to expand the binary array
    return permutated

def keyGenerator():
    #Read in key file should be shared secretly!
    f = open('key.txt', 'r')
    key = ''
    for i in f:
        key += i
    f.close()

    keyArr = []

    #Chose how many keys for cycles
    keysWanted = 2
    keys = []

    #Convert string to array
    for i in key:
        keyArr.append(int(i))
    key = permutation(key,1) #Permtation of 10bit key

    #Split key in half
    k0 = key[0:(len(key)/2)]
    k1 = key[(len(key)/2):]

    for i in range(0,keysWanted): #Rounds to generate keys
        #Left shift
        k0 = leftShift(k0)
        k1 = leftShift(k1)

        #Combine for condense permutation
        k2 = k0 + k1
        newKey = condensePermutation(k2)
        keys.append(newKey) #Key Generated
    return keys

def encrypt(arr,keys):
    print('Plaintext: ' + str(arr))
    arr = permutation(arr,0) #Initial Permutation

    #Initial slit of the array
    l0 = arr[0:(len(arr)/2)]
    r0 = arr[(len(arr)/2):]

    rounds = 2 #Setting how many rounds
    for i in range(0,rounds): #Cycles keys used in forward order
        if(i==rounds-1):
            r0, l0 = keyFunction(l0,r0,keys[i])
        else:
            l0, r0 = keyFunction(l0,r0,keys[i])

    arr = l0 + r0 #Combine arrays to return to normal size
    arr = permutation(arr,3) #Inverse Initial Permutation
    print('Ciphertext: ' + str(arr))
    return arr

def decrypt(arr,keys):
    print('Ciphertext: ' + str(arr))
    arr = permutation(arr,0) #Initial Permutation
    rounds = 2 #Setting how many rounds

    #Initial split of the array
    l0 = arr[0:(len(arr)/2)]
    r0 = arr[(len(arr)/2):]

    #Cycles but use key in reverse order
    for i in range(0,rounds):
        if(i==rounds-1):
            r0, l0 = keyFunction(l0,r0,keys[len(keys)-1-i])
        else:
            l0, r0 = keyFunction(l0, r0, keys[len(keys)-1-i])

    arr = l0 + r0 #Combine arrays to return to normal size
    arr = permutation(arr,3) #Inverse Initial Permutation
    print('Plaintext: ' + str(arr))
    return arr

def main(text, needEncrypt):
    #Read the file
    file = open(text, "r")
    message = ''
    for i in file:
        message += i
    file.close()

    if(len(message)!=8):
        print('Not an 8bit message')
        sys.exit()

    #Create bits into array
    arr = []
    for i in message:
        arr.append(int(i))

    #Get your keys!
    keys = keyGenerator()

    #Decide if you're encrypting or decrypting
    if(needEncrypt):
        arr = encrypt(arr,keys)
    else:
        arr = decrypt(arr,keys)
    return arr #Send back to client or server

#This was just for testing before setting up socket
if __name__ == "__main__":
    arr = main(sys.argv[1], True) #Takes in file from command line
    