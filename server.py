import socket
import security

if __name__ == "__main__":
    s = socket.socket()
    print('socket created')
    port = 12345

    s.bind(('', port))         
    print("socket binded to %s" %(port))
    
    # put the socket into listening mode 
    s.listen(5)      
    print("socket is listening")         
    

    f = open('messageToRecv.txt', 'w')
    while True: 
        # Establish connection with client. 
        c, addr = s.accept()      
        print('Got connection from', addr) 

        print('Receiving...')
        l = c.recv(1024)
        #Reads in the message from the client
        while(l):
            print "Receiving..."
            f.write(l)
            l = c.recv(1024)
        f.close()
        print('Received!')

        arr = security.main('messageToRecv.txt', False) #Decrypt

        # send a thank you message to the client.  
        c.send('Thank you for connecting') 

            
        
        # Close the connection with the client 
        print('Closing Connection...')
        c.close() 
        break