import socket               # Import socket module
import security
import sys

s = socket.socket()         # Create a socket object
host = '127.0.0.1'          # Get local machine name
port = 12345                 # Reserve a port for your service.

s.connect((host, port))
while(True):
    #Choose how you want to send a message
    choice = raw_input('File, or Input? (file/input): ')
    if(choice == 'file'):
        break
    elif(choice == 'input'):
        msg = raw_input('Type in bit message: ')
        f = open('MessageToSend.txt', 'w')
        f.write(msg)
        f.close()
        break
    else:
        print('Inalid input case sensative')
            
arr = security.main('MessageToSend.txt', True) #Encrypt message
f = open('EncryptedFile.txt', 'w')
for i in arr:
    f.write(str(i))
f.close()
f = open('EncryptedFile.txt','r')
print 'Sending...'
l = f.read(1024)

#send file to server
while (l):
    print 'Sending...'
    s.send(l)
    l = f.read(1024)
f.close()
print "Done Sending"

s.shutdown(socket.SHUT_WR) #Tell server you're done sending
print s.recv(1024)
s.close                     # Close the socket when done